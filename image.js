var fs = require('fs');
var path = require('path');
var existsSync = path.existsSync || fs.existsSync;
var exists = path.exists || fs.exists;



function Image(_name, _size){

	var parts = [];
	var name = _name;
	var size = _size;

	this.name = _name;
	this.size = _size;
	this.isElementExist = isElementExist;
	this.addElement = addElement;
	this.isComplete = isComplete;
	
	var cacheName = "." + this.name + ".cache";
	
	parts = loadCache(cacheName);
	

	return this;


	function loadCache(filename) {
		if (existsSync(filename)) {
			console.log("read cache file = " + filename);
			var data = fs.readFileSync(filename, 'utf8');
			return JSON.parse(data);
		} else {
			console.log("cache file doesn't exist. Create it");
			fs.writeFileSync(filename, "", 'utf8');
			return [];
		}
	}


	function isElementExist(number){
		if (parts[number]) {
			return true;
		}
		return false;
	}


	function addElement(_numer, _offset, _s, _data){
		var el = {
			offset: _offset,
			size: _s,
			data: _data
		};
		parts[_numer] = el;
		saveParts(cacheName);
	}


	function saveParts(filename) {
		var data = JSON.stringify(parts);

		fs.unlink(filename, function(err) {
			if (err) {
				console.log("Error while remiving file " + err.message);
			} else {
				fs.writeFile(filename, data, 'utf8', function(e) {
					if (e) {
						console.log("error while writing file " + e.message);
					}
				});
			}
		});
	}


	function isComplete() {
		var sum = 0;
		for(var i in parts) {
			if (i == 0) continue;
			if (!parts[i]) continue;

			sum += parts[i].size;			
		}
		console.log("file name = " + name + ", size = " + sum + ", all size = " + size);
		if (sum == this.size) {
			saveImageFile(this.name);
			return true;
		}
		return false;
	}

	function saveImageFile(filename) {
		var buffer;
		for(var i in parts) {
			if ( i == 0) continue;
			var b = new Buffer(parts[i].data, 'base64');
			buffer += b;
		}
		exists(filename, function (exist) {
			if (exist) {
				console.log("File = " + filename + " already exists");
			} else {
				fs.writeFile(filename, buffer, function (err) {
					if (err) {
						console.log("Can't write file = " + filename);
					} else {
						console.log("File = " + filename + " saved successfuly");
					}
				});
			}
		});
	}
}


exports.Image = Image;
