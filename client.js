var http = require("http");

function ServerHandler(server, checkFirstChunk, addImagePart, isDone) {
	var errorsCount = 0;
	var requestsCount = 0;

	var buf = "";

	this.errors = errors;
	this.requests = requests;
	this.start = sendRequest;
	return this;

	function sendRequest() {
		requestsCount++;
		buf = "";

		var req = http.get(server);
		req.on('error', receiveError);
		req.on('response', receiveResponse);
	}

	function receiveResponse(resp) {
		var i = 0;

		if (resp.statusCode == "200") {
			resp.on('data', function(chunk) {
				receiveChunk(resp, chunk, i);
				i++;
			});
		} else {
			allDataReceived(false);
		}
	}

	function receiveChunk(resp, chunk, n) {
		if (n == 0) {
			if (checkFirstChunk(chunk)) {
				buf += chunk;
				resp.on('end', function() {
					allDataReceived(true);
				});
			} else {
				resp.destroy();
				allDataReceived(false);
			}
		} else {
			buf += chunk;
		}
	}

	function allDataReceived(state) {
		if (state) {
			addImagePart(buf);
		} else {
			errorsCount++; 
		}
		if (isDone()) {
			console.log("------------------------------------------------------------");
			console.log("--------------------- " + server.path + " handler is closing");
			console.log("--------------------- " + server.requestsCount + " - requests");
			console.log("--------------------- " + server.errorsCount + " - errors");
		} else {
			sendRequest();
		}
	}

	function receiveError(err) {
		console.log("error = " + err.message);
		allDataReceived(false);
	}

	function errors() {
		return errorsCount;
	}

	function requests() {
		return requestsCount;
	}
}

exports.handle = ServerHandler;
