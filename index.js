var http = require('http');
var fs = require('fs');
var client = require('./client');
var gallery = require('./image');

var server = "89.253.235.155";
var port = 8080;
var method = "GET";
var images = [];
var saved = [];
var isCollected = false;
var errorCount = 0;


var endpoints = new Array(
	{ host: server, port: port, path: "/endpoint1", method: method },
	{ host: server, port: port, path: "/endpoint2", method: method },
	{ host: server, port: port, path: "/endpoint3", method: method },
	{ host: server, port: port, path: "/endpoint4", method: method },
	{ host: server, port: port, path: "/endpoint5", method: method }
);
	

function GetAllImages() {
	for(var i in endpoints) {
		var handler = client.handle(endpoints[i], checkFirst, addImagePart, isDone);
		handler.start();
		console.log("Endpoint " + endpoints[i].path + " is serving");
	}
}

function checkFirst(data) {
	str = String(data);

	var words = str.split(',');
	var name = words[2].split(':')[1].split('"')[1];
	var no = parseInt(words[4].split(':')[1]);
	if (images[name]) {
		if (images[name].isElementExist(no)) {
			console.log("part no:" + no + "\t-->>\t" + name + "\t-- NULL");
			return false;
		}
	}
	return true;
}

function addImagePart(data) {
	var el = JSON.parse(data);
	console.log("part no:" + el.imagePartNumber + "\t-->>\t" + el.imageName);

	if (!images[el.imageName]) {
		console.log("Image is not exist yet " + el.imageName);
		images[el.imageName] = new gallery.Image(el.imageName, el.sizeOfImageInBytes);
	}
	images[el.imageName].addElement(el.imagePartNumber, el.partOffset, 
		el.sizeOfPartInBytes, el.base64Data);
}

function isDone() {
	//console.log("check is done length = " + images.length);
	//if (images.length == 0) return false;

	for(var i in images) {
		if (!images[i].isComplete()) {
			console.log("image = " + images[i].name + " not complete");
			return false;
		}
	}
	console.log("---------------------------------------------------");
	console.log("Congradulations. All done.");
	return true;
}

// Start of the program
console.log(" ----------------------------- start ----------------------------");
GetAllImages();
